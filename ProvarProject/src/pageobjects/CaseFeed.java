package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="CaseFeed"                                
     , summary=""
     , relativeUrl=""
     , connection="DemoOrg"
     )             
public class CaseFeed {

	@TextType()
	@FindBy(xpath = "//div[contains(@class,'active') and contains(@class,'oneContent')]//span[normalize-space(.)='View Details']/lightning-primitive-icon")
	public WebElement infoIcon;
	@LinkType()
	@FindBy(xpath = "//div[contains(@class,'active') and contains(@class,'oneContent')]//a[normalize-space(.)='View Details' and @title='View Details']")
	public WebElement infoIcon1;
	@LinkType()
	@FindBy(xpath = "//a[normalize-space(.)='View Email']")
	public WebElement viewEmail;
			
}
