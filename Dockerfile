# Specify Provar base image version
ARG PROVAR_VERSION

FROM provartesting/ant-cli:${PROVAR_VERSION}

# Define environment variables
ARG PROJECT_NAME=ProvarProject
ARG BROWSER
ARG BUILD_FILE
ARG BUILD_FOLDER
ARG ENVR
ARG ANT_TARGET=runtests
ARG TEST_PLAN
ARG PROVAR_SECRETS_PASSWORD
ENV PROJECT_PATH=/home/${PROJECT_NAME} \
    DISPLAY=:99.0 \
    JAVA_ARGS=-verbose:class \
    PROVAR_AUTORETRY_TIMEOUT=30 \
    PROVAR_AUTORETRY_OVERRIDE=ENABLE_ALL \
    PROVAR_EXECUTION_MODE=Docker

# Copy Provar project and resources
COPY ${PROJECT_NAME} ${PROJECT_PATH}
COPY /.secure_files/Floating.properties ${PROVAR_HOME}/.licenses/

# Set the working directory
WORKDIR ${PROJECT_PATH}

# Define entrypoint for Provar execution
RUN echo "#!/bin/sh \n env \n xvfb-run ant -Dtestcycle.path=${PROJECT_PATH}/TestCycle/ -Dtest.plan=\"${TEST_PLAN}\" -Dtestproject.results=${PROJECT_PATH}/Results/ -Dprovar.home=${PROVAR_HOME} -Dsecrets.password=${PROVAR_SECRETS_PASSWORD} -Dtestenvironment.secretspassword=${PROVAR_SECRETS_PASSWORD} -Dbrowser=${BROWSER} -Denvr=${ENVR} -Dtestproject.home=${PROJECT_PATH} -f ${PROJECT_PATH}/${BUILD_FOLDER}/${BUILD_FILE} ${ANT_TARGET}" > ./entrypoint.sh
RUN chmod +x ./entrypoint.sh

ENTRYPOINT ["./entrypoint.sh"]
CMD